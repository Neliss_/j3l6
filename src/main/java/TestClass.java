public class TestClass {

    @BeforeSuite
    public static void start() {
        System.out.println("Начало");
    }

    //    @BeforeSuite
//    public static void start1() {
//        System.out.println("Начало");
//    }
//    @Test(priority = 5)
//    public static void test0() {
//        System.out.println("10");
//    }


    @Test(priority = 2)
    public static void test1() {
        System.out.println("1");
    }

    @Test(priority = 3)
    public static void test2() {
        System.out.println("2");
    }

    @Test(priority = 4)
    public static void test3() {
        System.out.println("3");
    }

    @AfterSuite
    public static void end() {
        System.out.println("Конец");
    }

}

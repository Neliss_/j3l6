import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Array;
import java.util.*;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        starting(TestClass.class);

    }

    public static void starting(Class c) throws NoSuchMethodException {
        Method[] methods = c.getDeclaredMethods();
        Map<Integer, Method> ts = new TreeMap();
        int min_priopity = 1;
        int max_priority = 10;
        int start = 0;
        int end = 0;
        for (Method o : methods) {
            if (o.isAnnotationPresent(BeforeSuite.class)) {
                start++;
                ts.put(min_priopity - 1, o);
            }
            if (o.isAnnotationPresent(AfterSuite.class)) {
                end++;
                ts.put(max_priority + 1, o);
            }
            if (start > 1) {
                throw new RuntimeException("Метод с аннотацией BeforeSuite должен присутствовать только 1 раз!");
            }
            if (end > 1) {
                throw new RuntimeException("Метод с аннотацией AfterSuite должен присутствовать только 1 раз!");
            }
            if (o.isAnnotationPresent(Test.class)) {
                int p = o.getAnnotation(Test.class).priority();
                ts.put(p, o);
            }
        }

        for (Integer key : ts.keySet()) {
            try {
                ts.get(key).invoke(null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }
}

